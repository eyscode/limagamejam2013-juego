import pyglet.window
from pyglet.gl.gl import glPushMatrix, glPopMatrix
from cocos.layer import Layer
from cocos.sprite import Sprite
from cocos.director import director
from cocos.scenes.transitions import *
from cocos.menu import *
from cocos.text import *

import pyglet
from gamelib import soundex

class CapaFondo(Layer):
    def __init__(self, level=None):
        super(CapaFondo, self).__init__()
        self.img = pyglet.resource.image(level.fondo) if level else None

    def agregar_objeto(self, objeto, z=0, nombre=None):
        self.add(objeto, z=z, name=nombre)

    def draw(self, *args, **kwargs):
        if self.img:
            glPushMatrix()
            self.transform()
            self.img.blit(0, 0)
            glPopMatrix()


class TitleLayer(Layer):
    def __init__(self, fondo=None):
        super(TitleLayer, self).__init__()

        w, h = director.get_window_size()
        self.menu = Sprite(fondo)
        self.menu.position = (400, 300)
        self.add(self.menu, z=0)