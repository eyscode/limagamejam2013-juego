from cocos.director import director
from pyglet import font
from gamelib.modelos.menu import get_intro_scene

import pyglet

def main():
    pyglet.resource.path.append('data')
    pyglet.resource.reindex()
    font.add_directory('data')

    director.init(width=1024, height=768)
    #
    #
    #director.set_depth_test(True)
    director.show_FPS = True

    s = get_intro_scene()
    director.run(s)

if __name__ == "__main__":
    main()
