#
# HiScore
#
import urllib2

__all__ = ['hiscore']

class HiScoreData(object):
    POINTS, NAME, LVL = range(3)

    HISCORE_FILENAME = 'hi_scores.txt'

    MAX = 10

    def __init__( self ):
        super(HiScoreData, self).__init__()
        self.linea = False
        self.load()

    def load(self):
        self.hi_scores = []
        #carga de internet
        if self.linea:
            try:
                import json
                import urllib2

                ptjs = json.load(urllib2.urlopen('http://kodevian.net/heartattack/api/nuevo'))
                print ptjs
                for p in ptjs:
                    self.hi_scores.append((p['nombre'], p['puntaje']))

            except Exception:
                self.linea = False
        if not self.linea:
            try:
                f = open(self.HISCORE_FILENAME)
                for line in f.readlines():
                    # ignore comments
                    if line.startswith("#"):
                        continue
                    (name, score) = line.split(',')
                    self.hi_scores.append((name, int(score)))
                f.close()
            except IOError:
                # file not found, no problem
                pass

    def save(self):
        try:
            f = open(self.HISCORE_FILENAME, 'w')
            for i in self.hi_scores:
                f.write('%d,%s,%d\n' % ( i[0], i[1], i[2] ))
            f.close()
        except Exception, e:
            print 'Could not save hi scores'
            print e

    def add( self, name, score):
        # safe replacement

        if self.linea:
            try:
                import json

                data = {
                    'nombre': name,
                    'puntaje': score
                }
                req = urllib2.Request('http://kodevian.net/heartattack/api/nuevo')
                req.add_header('Content-Type', 'application/json')

                response = urllib2.urlopen(req, json.dumps(data))
            except Exception:
                self.linea = False
        if not self.linea:
            for l in name:
                if not l.isalnum():
                    name = name.replace(l, '_')

            self.hi_scores.append((int(score), name, int(lvl) ))
            self.hi_scores.sort()
            self.hi_scores.reverse()
            self.hi_scores = self.hi_scores[:self.MAX]
            self.save()

    def is_in( self, score ):
        if len(self.hi_scores) < self.MAX:
            return True
        if score > self.hi_scores[-1][0]:
            return True
        return False

    def get( self, max=10 ):
        # only return the max first records
        return self.hi_scores[:max]

hiscore = HiScoreData()
