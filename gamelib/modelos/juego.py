# coding=utf-8
from cocos.actions.interval_actions import FadeOut, FadeIn
from cocos.layer import Layer
from cocos.sprite import Sprite
from cocos.scene import Scene
from gamelib import soundex
from gamelib.modelos import constantes, levels
from gamelib.modelos.organos import *
from gamelib.modelos.state import state
from gamelib.vistas.capas import CapaFondo
from gamelib.controladores.juego import CapaControl
from HUD import ScoreLayer

import random

class CapaModelo(Layer):
    def __init__(self, level=None):
        super(CapaModelo, self).__init__()

        self.fondo = Sprite(level.fondo)
        self.fondo.position = 512, 384
        self.add(self.fondo, z=5)
        self.mundo = Sprite(level.mundo)
        self.mundo.position = 512, 384
        self.add(self.mundo, z=-5)
        self.player = Corazon(position=level.position_player)
        self.add(self.player.sprite, z=10)
        self.schedule(self.update)
        #self.glow_a = [Sprite("higado-glow-a.png"), Sprite("cerebro-glow-a.png"), Sprite("pulmones-glow-a.png"),
        #               Sprite("estomago-glow-a.png"), Sprite("estomago-glow-a.png"), Sprite("rinon-glow-a.png")]
        #for ga in self.glow_a:
        #    ga.do(FadeOut(0))
        #    self.add(ga, 15)
        #    ga.position = 512, 384
        self.schedule_interval(self.update_2, interval=0.2)
        self.schedule_interval(self.generar_imagen, interval=1)
        self.organos = [Organo(*a) for a in level.organos]
        for org in self.organos:
            self.add(org.sprite, z=0)
        self.gotas = []
        self.level = level
        self.imagen = None
        self.tiempo = 0
        self.tiempo2 = 0
        self.duracion = 5
        self.next = True
        self.elegido = None
        self.count = 0

    def generar_imagen(self, dt):
        if state.state == state.STATE_PLAY:
            if self.next:
                self.tiempo += 1
            else:
                self.tiempo2 += 1
            if self.tiempo % 5 == 0 and self.tiempo > 0 and self.next == True:
                self.elegido = random.randint(0, 4)
                self.organos[self.elegido].velocity_fall *= 3
                imagenes = constantes.organos[self.elegido]
                imagen = imagenes[random.randint(0, len(imagenes) - 1)]
                self.imagen = Sprite(imagen)
                self.imagen.position = constantes.posiciones[self.elegido], 650
                self.add(self.imagen, z=20)
                self.do(FadeOut(0))
                self.do(FadeIn(1))
                #self.glow_a[self.elegido].do(FadeIn(0.5))
                self.next = False
                self.tiempo = 0
            if self.tiempo2 % self.duracion == 0 and self.tiempo2 > 0 and self.imagen and not self.next:
                self.imagen.do(FadeOut(1))
                #self.glow_a[self.elegido].do(FadeOut(0.5))
                self.next = True
                self.tiempo2 = 0
                self.organos[self.elegido].velocity_fall /= 3
            if state.state == state.STATE_PLAY:
                state.time += 1


    def on_enter(self):
        super(CapaModelo, self).on_enter()
        if self.level.music:
            soundex.set_music(self.level.music)
        state.state = state.STATE_PLAY

    def update(self, dt):
        if 0.03 < dt:
            self.player.velocity = 15
            Gota.velocity = 25
        elif 0.01 < dt <= 0.03:
            self.player.velocity = 6
            Gota.velocity = 10
        else:
            self.player.velocity = 4
            Gota.velocity = 5
        if state.state == state.STATE_PLAY:
            self.parent.get('ctrl').update_keys()
            for g in self.gotas:
                g.update()
                if g.sprite.position[1] > 700:
                    g.sprite.kill()
                    self.gotas.remove(g)
                    break
                for org in self.organos:
                    rect = org.rect.copy()
                    rect.bottom += 30
                    if rect.contains(g.sprite.position[0], g.sprite.position[1]):
                        org.llenar()
                        g.explotar()
                if g.exploto and g.sprite._frame_index + 1 == len(g.sprite.image.frames):
                    g.sprite.kill()
                    self.gotas.remove(g)
                    break

    def update_2(self, dt):
        for org in self.organos:
            org.update()

def get_game_scene():
    #state.reset()
    s = Scene()
    score = ScoreLayer()
    s.add(score, z=1, name='hud')
    if state.state == state.STATE_PAUSE:
        levelx = levels.levels[state.level - 1]
        gameModel = CapaModelo(levelx)
        gameControler = CapaControl(gameModel)
        s.add(gameModel, z=0, name='model')
        gameModel.atras = CapaFondo(levelx)
        s.add(gameControler, z=0, name='ctrl')
        s.add(gameModel.atras, z=-1, name='bg')
    return s