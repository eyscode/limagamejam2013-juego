# coding=utf-8
from pyglet.window.key import RIGHT, LEFT, UP, A, W, D, I, J, L
from os.path import realpath, join, dirname
#fuente = 'Gemina Expanded Italic'
fuente = 'Edit Undo Line BRK'
MUSIC, SOUND = True, True
RUTA = realpath(join(realpath(join(dirname(__file__), '../..')), 'data'))
organos = {
    0: ['pulmones1.png','pulmones2.png'],
    1: ['higado1.png','higado2.png'],
    2: ['rinon1.png','rinon2.png'],
    3: ['estomago1.png','estomago2.png'],
    4: ['cerebro1.png','cerebro2.png']
}

posiciones = {
    0: 113,
    1: 300,
    2: 500,
    3: 702,
    4: 910
}