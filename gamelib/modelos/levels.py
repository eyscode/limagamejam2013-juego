# coding=utf-8

class Level(object):
    mapa = "mapa.land"
    gravity = (0, -800)
    title = "Demo"
    mundo = "mundo1.png"
    centro = 400, 400
    atras = 'fondo'
    position_player = 100, 100
    fondo = 'fondo_1.jpg'
    music = None


class LevelMenu(Level):
    mapa = "levelmenu.land"
    title = "Menu Niveles"
    mundo = "mundolevels.png"
    fondo = "fondolevels.jpg"
    position_player = 140, 300
    centro = (400, 300)


class Level1(Level):
    title = "2 organos"
    mundo = "eys fondo2.png"
    fondo = "eys fondo.png"
    position_player = 130, 100
    music = "musica de fondo.mp3"
    organos = [((25, 332, 145, 234), 80, "higado"), ((215, 377, 185, 163), 80, "higado"),
        ((446, 377, 96, 138), 100, "riñon"), ((595, 379, 161, 153), 80, "estomago"),
        ((800, 363, 200, 180), 80, "cerebro")]

levels = [Level1]
