__all__ = ['state']
from pyglet.window.key import RIGHT, LEFT, UP, SPACE, DOWN

class State(object):
    STATE_PAUSE, STATE_PLAY, STATE_WIN, STATE_OVER = range(4)
    LANG_EN, LANG_ES = range(2)

    def __init__( self ):
        # stado
        self.state = self.STATE_PAUSE
        self.level = 1
        self.time = 0
        self.countdown_initial = 8
        self.countdown = 8
        self.SPACE = SPACE
        self.UP = UP
        self.RIGHT = RIGHT
        self.LEFT = LEFT
        self.DOWN = DOWN
        self.bajando = False

    def reset( self ):
        self.state = self.STATE_PAUSE

state = State()
