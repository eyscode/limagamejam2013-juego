# coding=utf-8
from os.  path import join
from cocos.actions.base_actions import Repeat
from cocos.actions.basegrid_actions import AccelDeccelAmplitude
from cocos.actions.grid3d_actions import Waves, Waves3D
from cocos.actions.interval_actions import RotateBy, AccelDeccel, Accelerate, MoveBy
from cocos.rect import Rect
from cocos.sprite import Sprite
import pyglet
from gamelib import soundex
from gamelib.modelos import constantes

class Corazon(object):
    def __init__(self, position):
        self.sprite = Sprite("heart.png")
        self.sprite.position = position
        self.position = position
        self.conectado = False
        self.velocity = 4
        self.derecha = True
        #sheet = pyglet.image.load('grillaheart.png')
        #sequence = pyglet.image.ImageGrid(sheet, 7, 13)
        #self.animations = dict()
        #self.animations['derecha'] = pyglet.image.Animation.from_image_sequence(trozo, tiempo)
        #self.animations['izquierda']
        #self.animations['detenido']
        #self.animations['bombeando']
        #self.animations['conectando']
        #self.animations['desconectando']
        #self.animations['disparando']

    def conectar(self):
        self.conectado = True
        self.sprite.do(Accelerate(MoveBy((0, -30), 0.05)))

    def desconectar(self):
        self.conectado = False
        self.sprite.do(Accelerate(MoveBy((0, 30), 0.05)))


class Organo(object):
    velocity_fall = 3


    def __init__(self, rect, percent=100, name="un organo x"):
        self.name = name
        self.sprite = Sprite("sangre.png")
        action1 = Waves(amplitude=10, hsin=True, vsin=False, waves=5, grid=(10, 10),
            duration=5)
        self.sprite.do(Repeat(action1))
        del action1
        self.rect = Rect(rect[0], rect[1], rect[2], rect[3])
        self.actual = self.rect.height * percent / 100
        self.sprite.position = self.rect.center[0], self.rect.bottom + self.actual - self.sprite.get_rect().height / 2

    def sangre_baja(self):
        return True if self.actual < self.rect.height * 30 / 100 else False

    def sangre_0(self):
        return True if self.actual <= 0 else False

    def update(self):
        #if self.name == "riñon": print self.actual
        if self.sprite.get_rect().top < self.rect.bottom:
            #print "sangre baja", self.sprite.position
            pass
        else:
            self.actual -= self.velocity_fall
            self.sprite.position = self.sprite.position[0], self.sprite.position[1] - self.velocity_fall


    def llenar(self):
        if self.sprite.get_rect().top > self.rect.top:
            pass
        else:
            self.actual += 2
            self.sprite.position = self.sprite.position[0], self.sprite.position[1] + 2


class Gota(object):
    velocity = 10

    def __init__(self, position):
        self.exploto = False
        #self.sprite = Sprite("gota.png")
        #self.sprite.do (Repeat(RotateBy(180, 1)))
        sheet = pyglet.image.load(join(constantes.RUTA, 'gota.png'))
        sequence = pyglet.image.ImageGrid(sheet, 1, 4)
        self.animations = dict()
        self.animations['normal'] = pyglet.image.Animation.from_image_sequence([sequence[0]], 0.1)
        self.animations['explota'] = pyglet.image.Animation.from_image_sequence(sequence[1:4], 0.1, loop=False)
        #action1 = Waves(amplitude=10, hsin=True, vsin=True, waves=5, grid=(10, 10),
        #    duration=2)
        self.sprite = Sprite(self.animations['normal'])
        self.sprite.position = position
        #self.sprite.do(Repeat(action1))

    def update(self):
        if not self.exploto:
            self.sprite.position = self.sprite.position[0], self.sprite.position[1] + self.velocity

    def explotar(self):
        if not self.exploto:
            self.sprite.image = self.animations['explota']
            self.exploto = True