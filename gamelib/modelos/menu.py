# coding=utf-8
from cocos.actions.interval_actions import Delay
from cocos.layer.base_layers import MultiplexLayer
from cocos.layer.util_layers import ColorLayer
from cocos.scene import Scene
from cocos.scenes.transitions import *
from cocos.menu import Menu, MenuItem, shake, shake_back, CENTER, BOTTOM, verticalMenuLayout
from cocos.director import director
from gamelib import soundex, hiscore
from gamelib.modelos.state import state
from gamelib.vistas.capas import CapaFondo
from gamelib.modelos.juego import *
from pyglet import font
from cocos.text import *
from pyglet.window import key
import pyglet

class BaseMenu(Menu):
    def __init__(self, titulo=''):
        super(BaseMenu, self).__init__(titulo)
        self.select_sound = soundex.load('move.mp3')
        self.font_title['font_name'] = constantes.fuente
        self.font_title['font_size'] = 72
        self.font_title['color'] = (255, 255, 255, 255)
        self.font_item['font_name'] = constantes.fuente
        self.font_item['font_size'] = 22
        self.font_item_selected['font_name'] = constantes.fuente
        self.font_item_selected['color'] = (255, 255, 255, 255)
        self.font_item_selected['font_size'] = 25

    def on_enter(self):
        super(BaseMenu, self).on_enter()
        soundex.set_music('Menu Heart Attack.mp3')
        state.state = state.STATE_PAUSE

    def create_menu(self, items, selected_effect=None, unselected_effect=None,
                    activated_effect=None, layout_strategy=verticalMenuLayout):
        z = 0
        for i in items:
            # calling super.add(). Z is important to mantain order
            self.add(i, z=z)
            i.position = (0, 38 * -(z - 1))
            i.activated_effect = activated_effect
            i.selected_effect = selected_effect
            i.unselected_effect = unselected_effect
            i.item_halign = self.menu_halign
            i.item_valign = self.menu_valign
            z += 1

        self._generate_title() # If you generate the title after the items
        # the V position of the items can't consider the title's height
        if items:
            self._build_items(layout_strategy)

    def _generate_title( self ):
        width, height = director.get_window_size()

        self.font_title['x'] = width // 2 + 30
        self.font_title['text'] = self.title
        self.title_label = pyglet.text.Label(**self.font_title)
        self.title_label.y = height - self.title_label.content_height // 2 - 66

        fo = font.load(self.font_title['font_name'], self.font_title['font_size'])
        self.title_height = self.title_label.content_height


class ScoresLayer(ColorLayer):
    FONT_SIZE = 22

    is_event_handler = True #: enable pyglet's events

    def __init__(self):
        w, h = director.get_window_size()
        super(ScoresLayer, self).__init__(32, 32, 32, 16, width=w, height=h - 97)

        self.font_title = {}

        # you can override the font that will be used for the title and the items
        self.font_title['font_name'] = 'Edit Undo Line BRK'
        self.font_title['font_size'] = 72
        #        self.font_title['color'] = (204,164,164,255)
        self.font_title['color'] = (255, 255, 255, 255)
        self.font_title['anchor_y'] = 'top'
        self.font_title['anchor_x'] = 'center'

        title = Label('HEARTATTACK', **self.font_title)

        title.position = (w / 2.0, h - 5)

        self.add(title, z=1)

        self.table = None

    def on_enter( self ):
        super(ScoresLayer, self).on_enter()
        scores = hiscore.hiscore.get()
        if self.table:
            self.remove_old()
        self.table = []
        for idx, s in enumerate(scores):
            pos = Label('%d:' % (idx + 1), font_name='Edit Undo Line BRK',
                font_size=self.FONT_SIZE,
                anchor_y='top',
                anchor_x='left',
                color=(255, 255, 255, 255))

            name = Label(s[0], font_name='Edit Undo Line BRK',
                font_size=self.FONT_SIZE,
                anchor_y='top',
                anchor_x='left',
                color=(255, 255, 255, 255))

            score = Label(str(s[1]), font_name='Edit Undo Line BRK',
                font_size=self.FONT_SIZE,
                anchor_y='top',
                anchor_x='right',
                color=(255, 255, 255, 255))

            self.table.append((pos, name, score))
        self.process_table()

    def remove_old( self ):
        for item in self.table:
            pos, name, score = item
            self.remove(pos)
            self.remove(name)
            self.remove(score)
        self.table = None

    def process_table( self ):
        w, h = director.get_window_size()

        for idx, item in enumerate(self.table):
            pos, name, score = item

            posy = h - 180 - ( (self.FONT_SIZE + 20) * idx )

            pos.position = ( 25, posy)
            name.position = ( 68, posy)
            score.position = ( w - 100, posy )

            self.add(pos, z=2)
            self.add(name, z=2)
            self.add(score, z=2)

    def on_key_press( self, k, m ):
        if k in (key.ENTER, key.ESCAPE, key.SPACE):
            self.parent.switch_to(0)
            return True

    def on_mouse_release( self, x, y, b, m ):
        self.parent.switch_to(0)
        return True


class CreditsLayer(ColorLayer):
    FONT_SIZE = 22

    is_event_handler = True #: enable pyglet's events

    def __init__(self):
        w, h = director.get_window_size()
        super(CreditsLayer, self).__init__(32, 32, 32, 16, width=w, height=h - 97)

        self.font_title = {}

        # you can override the font that will be used for the title and the items
        self.font_title['font_name'] = 'Edit Undo Line BRK'
        self.font_title['font_size'] = 72
        #        self.font_title['color'] = (204,164,164,255)
        self.font_title['color'] = (255, 255, 255, 255)
        self.font_title['anchor_y'] = 'top'
        self.font_title['anchor_x'] = 'center'

        title = Label('HEARTATTACK', **self.font_title)

        title.position = (w / 2.0, h - 5)

        self.add(title, z=1)

        philip = Label('Philip Chu Joy', font_name='Edit Undo Line BRK',
            font_size=self.FONT_SIZE,
            anchor_y='top',
            anchor_x='left',
            color=(255, 255, 255, 255))

        eys = Label(u'Eysenck Gómez', font_name='Edit Undo Line BRK',
            font_size=self.FONT_SIZE,
            anchor_y='top',
            anchor_x='left',
            color=(255, 255, 255, 255))

        ale = Label(u'María ALejandra Gómez', font_name='Edit Undo Line BRK',
            font_size=self.FONT_SIZE,
            anchor_y='top',
            anchor_x='left',
            color=(255, 255, 255, 255))

        philip.position = 400, 450
        eys.position = 400, 350
        ale.position = 350, 250
        self.add(philip, z=2)
        self.add(eys, z=2)
        self.add(ale, z=2)

    def on_key_press( self, k, m ):
        if k in (key.ENTER, key.ESCAPE, key.SPACE):
            self.parent.switch_to(0)
            return True

    def on_mouse_release( self, x, y, b, m ):
        self.parent.switch_to(0)
        return True


class MainMenu(BaseMenu):
    def __init__(self):
        super(MainMenu, self).__init__()
        self.menu_anchor_y = BOTTOM
        self.menu_anchor_x = CENTER

        items = []
        self.title = "HEARTATTACK"
        items.append(MenuItem('New Game', self.on_new_game))
        items.append(MenuItem('Scores', self.on_scores))
        items.append(MenuItem('Credits', self.on_credits))
        items.append(MenuItem('Quit', self.on_quit))
        self.position = (0, -80)
        self.create_menu(items, shake(), shake_back())

    def on_new_game(self):
        director.push(FadeTransition(
            get_game_scene(), 1.0))

    def on_quit(self):
        pyglet.app.exit()

    def on_scores( self ):
        self.parent.switch_to(1)

    def on_credits(self):
        self.parent.switch_to(2)


class IntroLayer(Layer):
    is_event_handler = True

    def __init__(self):
        super(Layer, self).__init__()
        img = Sprite("inicio.png")
        img.do(FadeIn(2) + Delay(6) + FadeOut(2))
        img.position = 512, 384
        self.add(img)
        self.final = False
        self.schedule_interval(self.fin, 10)

    def fin(self, dt):
        if not self.final:
            self.final = True
            director.replace(FadeTransition(
                get_menu_scene(), 1.0))

    def on_key_press( self, k, m ):
        if k in (key.ENTER, key.ESCAPE, key.SPACE):
            self.fin(0)
            return True


def get_intro_scene():
    s = Scene()
    s.add(IntroLayer())
    return s

def get_menu_scene():
    scene = Scene()
    fondo = CapaFondo()
    fondo.img = pyglet.resource.image('menufondo.jpeg')
    #pisos = Sprite('mundo1.png')
    #saltos = MoveBy((100, 400), 5) + MoveBy((100, -400), 5) + MoveBy((100, 400), 5)
    #fondo.agregar_objeto(pisos, nombre='pisos')
    #fondo.get('pisos').position = (-100, 100)
    #fondo.get('pisos').do(Repeat(saltos + Reverse(saltos)))
    scene.add(MultiplexLayer(
        MainMenu(),
        ScoresLayer(),
        CreditsLayer(),
    ),
        z=2)
    scene.add(fondo, z=0)
    #tituloMain = TitleLayer('menuprincipal.png')
    #tituloSub = TitleLayer('submenu.png')
    #scene.add(tituloMain, z=1, name='tltm')
    #scene.add(tituloSub, z=1, name='tlto')
    #tituloSub.visible = False
    return scene