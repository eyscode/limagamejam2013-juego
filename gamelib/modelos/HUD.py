from cocos.layer import *
from cocos.text import *
from cocos.actions import *
from state import state

class ScoreLayer(Layer):
    def __init__(self):
        w, h = director.get_window_size()
        super(ScoreLayer, self).__init__()

        # transparent layer
        self.add(ColorLayer(32, 32, 32, 32, width=w, height=48), z=-1)

        self.position = (0, h - 48)

        self.time = Label('Time:', font_size=36,
            font_name='Edit Undo Line BRK',
            color=(255, 255, 255, 255),
            anchor_x='left',
            anchor_y='bottom')
        self.time.position = (800, -10)
        self.add(self.time)
        self.count = Label('Countdown:', font_size=36,
            font_name='Edit Undo Line BRK',
            color=(255, 255, 255, 255),
            anchor_x='left',
            anchor_y='bottom')
        self.count.position = (480, -445)
        self.add(self.count)
        self.count.do(FadeOut(0))

    def draw(self):
        super(ScoreLayer, self).draw()
        self.time.element.text = 'Time: %d' % state.time
        self.count.element.text = '0:0%d' % state.countdown

    def show(self):
        self.count.do(FadeIn(0.5))

    def hide(self):
        self.count.do(FadeOut(0.5))


class MessageLayer(Layer):
    def show_message( self, msg, callback=None ):
        w, h = director.get_window_size()

        self.msg = Label(msg,
            font_size=42,
            font_name='Edit Undo Line BRK',
            color=(64, 64, 64, 255),
            anchor_y='center',
            anchor_x='center')
        self.msg.position = (w / 2.0, h)

        self.msg2 = Label(msg,
            font_size=42,
            font_name='Edit Undo Line BRK',
            color=(255, 255, 255, 255),
            anchor_y='center',
            anchor_x='center')
        self.msg2.position = (w / 2.0 + 2, h + 2)

        self.add(self.msg, z=1)
        self.add(self.msg2, z=0)

        actions = Accelerate(MoveBy((0, -h / 2.0), duration=0.5)) +\
                  Delay(1) +\
                  Accelerate(MoveBy((0, -h / 2.0), duration=0.5)) +\
                  Hide()

        if callback:
            actions += CallFunc(callback)

        self.msg.do(actions)
        self.msg2.do(actions)


class HUD(Layer):
    def __init__( self ):
        super(HUD, self).__init__()
        self.add(ScoreLayer())
        self.add(MessageLayer(), name='msg')

    def show_message( self, msg, callback=None ):
        self.get('msg').show_message(msg, callback)
