from cocos.layer import Layer
from gamelib import soundex, gameover
from gamelib.modelos import constantes
from gamelib.modelos.organos import Gota
from gamelib.modelos.state import state
import pyglet

class CapaControl(Layer):
    is_event_handler = True

    def __init__(self, model):
        super(CapaControl, self).__init__()
        self.model = model
        self.schedule_interval(self.update_3, interval=0.6)
        self.schedule_interval(self.count_down, interval=1.0)
        self.on_keys = {'direction': list(), 'shoots': list(), 'connections': list(), 'desconnections': list()}

    def game_over(self):
        if state.state == state.STATE_PLAY:
            state.state = state.STATE_OVER
            self.parent.get('hud').hide()
            self.parent.add(gameover.GameOver(), z=10)

    def count_down(self, dt):
        if state.state == state.STATE_PLAY and state.bajando:
            if state.countdown == state.countdown_initial and self.parent.get('hud').count.opacity == 0:
                self.parent.get('hud').show()
            if self.parent.get('hud').count.opacity > 0:
                state.countdown -= 1
        if state.state == state.STATE_PLAY and not state.countdown:
            self.game_over()

    def update_3(self, dt):
        b = False
        for org in self.model.organos:
            if org.sangre_0():
                b = True
                state.bajando = True
                break
        if b and state.state != state.STATE_OVER:
            soundex.play("Beep2.mp3")
        elif not b:
            state.bajando = False
            if self.parent.get('hud').count.opacity > 0:
                self.parent.get('hud').hide()
            state.countdown = state.countdown_initial

    def on_key_press(self, key, modifiers):
        if key == state.SPACE:
            self.on_keys['shoots'].append(key)
        elif key == state.DOWN:
            self.on_keys['connections'].append(key)
        elif key == state.UP:
            self.on_keys['desconnections'].append(key)
        elif key in (state.RIGHT, state.LEFT):
            if key == state.RIGHT:
                self.on_keys['direction'].append(pyglet.window.key.RIGHT)
            elif key == state.LEFT:
                self.on_keys['direction'].append(pyglet.window.key.LEFT)

    def on_key_release(self, key, modifiers):
        if key == state.SPACE:
        #conectarse al organo
            try:
                self.on_keys['shoots'].remove(key)
            except  Exception, ex:
                self.on_keys['shoots'] = list()
        elif key == state.DOWN:
            try:
                self.on_keys['connections'].remove(key)
            except  Exception, ex:
                self.on_keys['connections'] = list()
        elif key == state.UP:
            try:
                self.on_keys['desconnections'].remove(key)
            except  Exception, ex:
                self.on_keys['desconnections'] = list()
        elif key in (state.RIGHT, state.LEFT):
            try:
                self.on_keys['direction'].remove(key)
            except Exception, ex:
                self.on_keys['direction'] = list()

    def update_keys(self):
        if len(self.on_keys['shoots']):
            if self.model.player.conectado:
                p = self.model.player.sprite.position
                soundex.play("splat.mp3")
                if self.model.player.derecha:
                    self.model.player.derecha = False
                    g = Gota((p[0] - 20, p[1]))
                else:
                    self.model.player.derecha = True
                    g = Gota((p[0] + 20, p[1]))
                self.model.add(g.sprite, z=9)
                self.model.gotas.append(g)
                self.on_keys['shoots'].pop(0)

        if len(self.on_keys['direction']):
            #if self.on_keys['direction'][-1] != self.model.player.direction:
            #    pass #animar a la derecha o izquierda

            #aqui camina el personaje
            if not self.model.player.conectado:
                pos = self.model.player.sprite.position
                if self.on_keys['direction'][-1] == constantes.LEFT:
                    if self.model.player.sprite.position[0] > 132:
                        p = self.model.player.sprite.position
                        self.model.player.sprite.position = p[0] - 190, p[1]
                if self.on_keys['direction'][-1] == constantes.RIGHT:
                    if self.model.player.sprite.position[0] < 800:
                        p = self.model.player.sprite.position
                        self.model.player.sprite.position = p[0] + 190, p[1]
                self.on_keys['direction'].pop(0)
        if len(
            self.on_keys['connections']) and not self.model.player.conectado and not self.model.player.sprite.actions:
            self.model.player.conectar()
            soundex.play("Atach.mp3")
            self.on_keys['connections'].pop(0)
        elif len(
            self.on_keys['desconnections']) and self.model.player.conectado and not self.model.player.sprite.actions:
            self.model.player.desconectar()
            soundex.play("Detach.mp3")
            self.on_keys['desconnections'].pop(0)